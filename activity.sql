-- Create DB
CREATE DATABASE blog_db;

-- Use DB
USE blog_db;

-- Create the Tables
CREATE TABLE users (
    id INT NOT NULL AUTO_INCREMENT,
    email VARCHAR(50) NOT NULL,
    password VARCHAR(50) NOT NULL,
    datetime_created DATETIME NOT NULL,
    PRIMARY KEY (id)
);

CREATE TABLE posts (
    id INT NOT NULL AUTO_INCREMENT,
    user_id INT NOT NULL,
    title VARCHAR(50) NOT NULL,
    content VARCHAR(50) NOT NULL,
    datetime_posted DATETIME NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT fk_posts_user_id
        FOREIGN KEY (user_id) REFERENCES users(id)
        ON UPDATE CASCADE
        ON DELETE RESTRICT
);


-- Add the following records to the blog_db database:

-- ○ Users
-- Email Password Datetime Created
-- johnsmith@gmail.com passwordA 2021-01-01 01:00:00
-- juandelacruz@gmail.com passwordB 2021-01-01 02:00:00
-- janesmith@gmail.com passwordC 2021-01-01 03:00:00
-- mariadelacruz@gmail.com passwordD 2021-01-01 04:00:00
-- johndoe@gmail.com passwordE 2021-01-01 05:00:00

INSERT INTO users (id, email, password, datetime_created) VALUES (1, "johnsmith@gmail.com", "passwordA", "2021-01-01 01:00:00");
INSERT INTO users (id, email, password, datetime_created) VALUES (2, "juandelacruz@gmail.com", "passwordB", "2021-01-01 02:00:00");
INSERT INTO users (id, email, password, datetime_created) VALUES (3, "janesmith@gmail.com", "passwordC", "2021-01-01 03:00:00");
INSERT INTO users (id, email, password, datetime_created) VALUES (4, "mariadelacruz@gmail.com", "passwordD", "2021-01-01 04:00:00");
INSERT INTO users (id, email, password, datetime_created) VALUES (5, "johndoe@gmail.com", "passwordE", "2021-01-01 05:00:00");


-- Add the following records to the blog_db database:
-- ○ Posts
-- User ID Title Content Datetime Posted
-- 1 First Code Hello World! 2021-01-02 01:00:00
-- 1 Second Code Hello Earth! 2021-01-02 02:00:00
-- 2 Third Code Welcome to Mars! 2021-01-02 03:00:00
-- 4 Fourth Code Bye bye solar system! 2021-01-02 04:00:00

INSERT INTO posts (id, user_id, title, content, datetime_posted) VALUES (1, 1, "First Code", "Hello World!", "2021-01-02 01:00:00");
INSERT INTO posts (id, user_id, title, content, datetime_posted) VALUES (2, 1, "Second Code", "Hello Earth!", "2021-01-02 02:00:00");
INSERT INTO posts (id, user_id, title, content, datetime_posted) VALUES (3, 2, "Third Code", "Welcome to Mars!", "2021-01-02 03:00:00");
INSERT INTO posts (id, user_id, title, content, datetime_posted) VALUES (4, 4, "Fourth Code", "Bye bye solar system!", "2021-01-02 04:00:00");


-- ● Get all the post with an Author ID of 1.
SELECT title, content, datetime_posted FROM posts WHERE user_id = 1;

-- ● Get all the user's email and datetime of creation.
SELECT email, datetime_created FROM users;

-- ● Update a post's content to "Hello to the people of the Earth!” where its initial content is "Hello Earth!" by using the record's ID.
UPDATE posts SET content = "Hello to the people of the Earth!" WHERE id = 2;

-- ● Delete the user with an email of "johndoe@gmail.com".
DELETE FROM users WHERE email = "johndoe@gmail.com";